## Tools

https://code.visualstudio.com/

https://platformio.org/platformio-ide


## Documentation

https://www.waveshare.com/wiki/AlphaBot

https://www.makerspaces.com/arduino-uno-tutorial-beginners/

https://www.arduino.cc/reference/en/

### Servo

https://www.pololu.com/product/1046

https://www.arduino.cc/en/Tutorial/Sweep

### Led (WS2812)

http://www.jarzebski.pl/arduino/komponenty/diody-led-ze-sterownikem-ws2811-ws2812.html

https://github.com/adafruit/Adafruit_NeoPixel

### 3d printing

http://thingiverse.com/

https://www.prusa3d.com/drivers/ -- look for "prusa control"

### Laser cutting

http://www.makercase.com/

https://makeabox.io/